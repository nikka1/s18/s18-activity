function Pokemon(name, level, health, attack) {
		// properties
		this.name = name;
		this.level = level;
		this.health = health;
		this.attack = attack;


		this.tackle = function(target){
			for (let targetHealth = target.health; targetHealth >= 5; targetHealth--) {
				if (targetHealth > 5) {
					console.log(this.name + ' tackled ' + target.name);
					console.log(target.name + "'s health is reduced by " + this.attack + " points");
					continue;
				}
				if (targetHealth <=5) {
					console.log("Health is already less than 5. " + target.name + " fainted.");
					break;
				}
			}
		}
		
	}

	let pikachu = new Pokemon("Pikachu", 10, 50, 5);
	let squirtle = new Pokemon("Squirtle", 9, 50, 4);

	pikachu.tackle(squirtle);
